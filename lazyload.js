/**
 * Pour le lazyloading des images, il faut rajouter la class lazy sur les images
 * Pour le lazyload des images en css, la class .lazy-background va charger l'image de remplacement. le script va ajouter une class
 * visible qui va charger l'image désiré en différé
 * Le chargement s'effectue au scroll lorsque que l'élément chargé en differé arrive à 100 px du viewport
 */
 
document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
  var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));
  var options = {
    rootmargin : '100px'
  }

  if("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if(entry.intersectionRatio > 0 ) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.srcset = lazyImage.dataset.src;
          lazyImage.classList.remove("lazy");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    }, options);

    let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if(entry.intersectionRatio > 0 ) {
          entry.target.classList.add("visible");
          lazyBackgroundObserver.unobserve(entry.target);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });

    lazyBackgrounds.forEach(function(lazyBackground) {
      lazyBackgroundObserver.observe(lazyBackground);
    });

  } else { // IE11
    lazyImages.forEach(function(img) {
      img.src = img.getAttribute("data-src");
      img.srcset = img.src;
      img.classList.remove("lazy");
    });

  }

})